import 'package:catatan_harian/addTask.dart';
import 'package:catatan_harian/main.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_sign_in/google_sign_in.dart';

class MyCounting extends StatefulWidget {
  MyCounting({this.user, this.googleSignIn});
  final FirebaseUser user;
  final GoogleSignIn googleSignIn;
  @override
  _MyCountingState createState() => _MyCountingState();
}

class _MyCountingState extends State<MyCounting> {

  void _signOut(){
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 215.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new Text(' Notifikasi ', style: TextStyle(fontSize: 25.0, fontFamily: "IdealistSans"),),
            new Text(' Anda yakin ingin keluar dari aplikasi ? ', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w400), textAlign: TextAlign.center,),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded( 
                  child: new RaisedButton(
                    padding: const EdgeInsets.all(8.0),
                    textColor: Colors.white,
                    color: Colors.green,
                    onPressed: (){
                      widget.googleSignIn.signOut();
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context)=> new MyHomePage())
                      );
                    },
                    child: new Text("Ya"),
                  ),
                ),
                new Padding(padding: new EdgeInsets.only(left: 10.0, right: 10.0)),
                Expanded(
                  child: new RaisedButton(
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                    textColor: Colors.white,
                    color: Colors.red,
                    padding: const EdgeInsets.all(8.0),
                    child: new Text(
                      "Tidak",
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
    
    showDialog(context: context, child: alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 170.0,
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: new AssetImage("img/tri.jpg"), fit: BoxFit.cover),
          boxShadow: [
            new BoxShadow(
              color: Colors.grey,
              blurRadius: 8.0,
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: new Row(
            children: <Widget>[
              Container(
                width: 60.0,
                height: 60.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: new NetworkImage(widget.user.photoUrl), fit: BoxFit.cover),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(" Selamat Datang! ", style: new TextStyle(fontSize: 20.0, color: Colors.grey),),
                      new Text(widget.user.displayName, style: new TextStyle(fontSize: 35.0, color: Colors.grey),),
                    ],
                  ),
                ),
              ),
              new IconButton(
                  icon: Icon(Icons.exit_to_app, color: Colors.grey, size: 40.0,),
                  onPressed: _signOut
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        child: Icon(Icons.add, color: Colors.white,),
          backgroundColor: Colors.grey[600],
          onPressed: (){
            Navigator.of(context).push(
                MaterialPageRoute(builder: (BuildContext context)=> new AddTask())
            );
      }),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: new BottomAppBar(
        elevation: 20.0,
        color: Colors.grey[600],
        child: ButtonBar(
          children: <Widget>[],
        ),
      ),
    );
  }
}
